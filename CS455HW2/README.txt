======
README
======

This document contains information on how to run the software submitted:

-----------------
BUILDING PROJECT
-----------------

Inside the tar file submission, please go to the path /CS455HW2/src. This is the path that contains the Makefile.

In order to build the project and generate the necessary class files, run the following command from the /CS455HW2/src path:

"make all"


In order to clean up previously generated class files, please run "make clean" from the same path.


---------------------
RUNNING THE SOFTWARE
---------------------

You can run either spawn a Server or a Client using the software. These two types of nodes constitute the two main starting points for execution of this project.

There can be one Server and multiple Client instances.

The command to start a server is : java cs455.scaling.server.Server <portnum> <thread-pool-size>

The command to spawn a Client is : java cs455.scaling.client.Client <server-host> <server-port> <message-rate>


---------------------
CHECKING STATISTICS
---------------------

Since intermediate actions on both the Client(s) and the Server get logged to the console, keeping track of the statistics messages could be hard. In case you want to check the statistics generated at each node, Client or Server, look into the following file:

1) For Client: Client diagnostics reports are found in files by the name client-diagnostic-<MACHINE_NAME>.out. If you have started multiple Client instances on a node, you will find numbers appended to the end of the filenames.
2) For Server: Server diagnostics reports are found in files by the name server-diagnostic.out

Both of the file types mentioned above will be found in the directory : /CS455HW2/logs 

------
NOTE:
------

This software implements all the functionality required in the Assignment. 



