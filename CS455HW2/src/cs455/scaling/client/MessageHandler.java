package cs455.scaling.client;

import java.util.List;

public class MessageHandler {
	
	private byte[] msg = null;
	private SendingThread st;
	
	public MessageHandler(SendingThread st) {
		this.st = st;
	}
	
	public synchronized boolean addMessage(byte[] msg) {
		
		this.msg = msg;
		/*notify();*/
		return handleMessage();
	}
	
	public synchronized boolean handleMessage() {
		
		/*while (msg == null) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println("PROBLEM IN MESSAGE HANDLING");
			}
		}*/
		
		String receivedBytes = new String(this.msg);
		
		synchronized (st.hashCodesList) {
			if (st.hashCodesList.contains(receivedBytes)) {
				System.out.println("FOUND MATCHING HASHCODE. REMOVING FROM LIST !!!! " +new String(this.msg));
				st.hashCodesList.remove(receivedBytes);
			} else {
				System.out.println("UNIDENTIFIED HASHCODE RECEIVED " + new String(this.msg));
			}
		}
		//System.out.println(new String(this.msg));
		
		return true;
	}

}