package cs455.scaling.client;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.logging.Logger;

import cs455.scaling.server.ServerDiagnostics;
import cs455.scaling.util.LogFactory;

public class ClientDiagnostics implements Runnable {
	
	private Client client;
	private Logger logger;

	public ClientDiagnostics(Client client) {
		try {
			this.logger = LogFactory.getLogger(ClientDiagnostics.class.getName(), "client-diagnostic-"+InetAddress.getLocalHost().getHostName()+".out");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.client = client;
	}

	@Override
	public void run() {
		while(true) {
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				
			}
			int numM;
			
			
			
			
			String s1 = "\n\n\n=========================================================================================================================\n";
			
			Timestamp t = new Timestamp(System.currentTimeMillis());
			
			String str = "["+ t.toString() + "] Total Sent Count: "+ client.sentMessageCount + " , Total Received Count: "+ client.receivedMessageCount;
			
			str = s1+str+"\n=========================================================================================================================\n\n\n";
			System.out.println(str);
			
			this.logger.info(str);
			
		}

	}

}
