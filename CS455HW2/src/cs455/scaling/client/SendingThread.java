package cs455.scaling.client;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import cs455.scaling.data.GenerateRandomString;
import cs455.scaling.data.SHA1Hasher;
import cs455.scaling.server.Server;
import cs455.scaling.util.LogFactory;

public class SendingThread implements Runnable{

	private static Logger logger;
	private int sendingRate;
	private Client callingNode;
	private SocketChannel clientChannel;
	private SelectionKey sKey;
	
	/* The number of messages the client is sending out */
	private int numSent;
	public List<String> hashCodesList;
	
	public SendingThread(int sendingRate, Client callingNode, SocketChannel clientChannel, SelectionKey sKey) {
		logger = LogFactory.getLogger(SendingThread.class.getName(), "sending.out");
		this.hashCodesList = new ArrayList<String>();
		this.numSent = 0;
		this.sendingRate = sendingRate;
		this.callingNode = callingNode;
		this.clientChannel = clientChannel;
		this.sKey = sKey;
		
	}
	
	
	public int getSentMessageCount() {
		return numSent;
	}
	
	@Override
	public void run() {
		
		while(true) {
			byte[] payload = GenerateRandomString.getString();
			//logger.info("About to send: " + payload.length+" bytes");
			String msgHashcode;
			try {
				msgHashcode = SHA1Hasher.SHA1FromBytes(payload);
				
				/* STORING HASHCODE FOR FUTURE REFERRAL */
				hashCodesList.add(msgHashcode);
			} catch (NoSuchAlgorithmException e1) {
				e1.printStackTrace();
				System.out.println("PROBLEM IN HASH CREATION");
			}
			
			MessageHandler handler = new MessageHandler(this);
			try {
				callingNode.send(clientChannel, sKey, payload, handler);
			} catch (IOException e) {
				e.printStackTrace();
			}
			//handler.handleMessage(this);
			
			numSent++;
			
			try {
				Thread.sleep(1000/sendingRate);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}