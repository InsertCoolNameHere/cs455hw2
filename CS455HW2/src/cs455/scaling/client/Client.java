package cs455.scaling.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import cs455.scaling.server.Server;
import cs455.scaling.util.LogFactory;

public class Client implements Runnable{
	
	private static Logger logger;
	private InetAddress serverIPAddr;
	private String serverHostName;
	private int serverPort;
	private int msgRate;
	private Selector clientSelector;
	private Map<SocketChannel, MessageHandler> channelToHandlerMap;
	private Map pendingData;
	private ByteBuffer readBuffer;
	public int receivedMessageCount = 0;
	public int sentMessageCount = 0;
	
	
	public static void main(String arg[]) {
		
		Client c = new Client(arg[0], arg[1], arg[2]);
		Thread cThread = new Thread(c);
		cThread.start();
		
	}
	
	
	public Client(String serverHostName, String serverPort, String rate) {
		try {
			Random r = new Random();
			int k = r.nextInt(1000);
			logger = LogFactory.getLogger(Client.class.getName(), "scaling-client"+k+".out");
			
			this.pendingData = new HashMap();
			this.channelToHandlerMap = new HashMap<SocketChannel, MessageHandler>();
			this.serverHostName = serverHostName;
			this.serverIPAddr = InetAddress.getByName(serverHostName);
			this.serverPort = Integer.valueOf(serverPort);
			this.msgRate = Integer.valueOf(rate);
			try {
				clientSelector = Selector.open();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void run() {
		
		/* INITIALIZING THE CLIENT SOCKET CHANNEL */
		
		SocketChannel socketChannel = null;
		try {
			
			socketChannel = SocketChannel.open();
			socketChannel.configureBlocking(false);
			socketChannel.connect(new InetSocketAddress(serverIPAddr, serverPort));
			socketChannel.register(this.clientSelector, SelectionKey.OP_CONNECT);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.info("STARTING CHANNEL SELECTION");
		/* STARTING ITERATION OVER THE SELECTOR */
		while(true) {
			try {
				
				clientSelector.select();
				
				Set<SelectionKey> selectedKeys = clientSelector.selectedKeys();

				Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
				
				while(keyIterator.hasNext()) {
					
					SelectionKey sKey = keyIterator.next();
					
					if(!sKey.isValid()) {
						continue;
					}
					if(sKey.isConnectable()) {
						
						finishConnection(sKey);
						
					} else if(sKey.isReadable()) {
						
						read(sKey);
					}
					
					keyIterator.remove();
					
				}
				
				
			} catch (IOException e) {
				
				logger.severe("TROUBLE DURING SELECTION IN CLIENT");
				e.printStackTrace();
				
			}
			
			
		}
		
		
	}


	private void read(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();
		readBuffer = ByteBuffer.allocate(2);
		ByteBuffer buff = null; 

		// Clear out our read buffer so it's ready for new data
		this.readBuffer.clear();

		// Attempt to read off the channel
		int numRead = 0;
		try {
			numRead = socketChannel.read(readBuffer);
			
			if(numRead == 2)
			{
				String size = new String(readBuffer.array());
				buff = ByteBuffer.allocate(Integer.parseInt(size));
				numRead = 0;
				numRead = socketChannel.read(buff); 
			}
			
		} catch (IOException e) {
			// The remote forcibly closed the connection, cancel
			// the selection key and close the channel.
			key.cancel();
			socketChannel.close();
			return;
		}
		//readBuffer.flip();
		if (numRead == -1) {
			// Remote entity shut the socket down cleanly. Do the
			// same from our end and cancel the channel.
			key.channel().close();
			key.cancel();
			return;
		}

		System.out.println("Length read: " + numRead);
		
		// Handle the response
		handleResponse(socketChannel, buff.array(), numRead);
		buff.clear();
		
		//key.interestOps(SelectionKey.OP_WRITE);
		
	}
	
	private void handleResponse(SocketChannel socketChannel, byte[] data, int numRead) throws IOException {
		// Make a correctly sized copy of the data before handing it
		// to the client
		byte[] rspData = new byte[numRead];
		System.arraycopy(data, 0, rspData, 0, numRead);
		
		// Look up the handler for this channel
		MessageHandler handler = (MessageHandler) this.channelToHandlerMap.get(socketChannel);
		
		// And pass the response to it
		if (handler.addMessage(rspData)) {
			receivedMessageCount++;
			// The handler has seen enough, close the connection
			//socketChannel.close();
			//socketChannel.keyFor(this.selector).cancel();
			//socketChannel.keyFor(this.selector).interestOps(SelectionKey.OP_WRITE);
		}
	}
	
	public void send(SocketChannel socket, SelectionKey key, byte[] data, MessageHandler handler) throws IOException {
		
		channelToHandlerMap.put(socket, handler);
		
		// And queue the data we want written
		synchronized (this.pendingData) {
			List queue = (List) this.pendingData.get(socket);
			if (queue == null) {
				queue = new ArrayList();
				this.pendingData.put(socket, queue);
			}
			queue.add(ByteBuffer.wrap(data));
		}

		// Finally, wake up our selecting thread so it can make the required changes
		//this.selector.wakeup();
		write(key);
	}
	
	private void write(SelectionKey key) throws IOException {
		
		SocketChannel socketChannel = (SocketChannel) key.channel();

		synchronized (this.pendingData) {
			
			List queue = (List) this.pendingData.get(socketChannel);

			/* WRITE OUT THE WHOLE QUEUE */
			
			while (!queue.isEmpty()) {
				
				ByteBuffer buf = (ByteBuffer) queue.get(0);
				
				
				//logger.info("I AM SENDING "+ buf.array());
				socketChannel.write(buf);
				
				logger.info("I HAVE SENT "+ buf.array());
				sentMessageCount++;
				
				
				if (buf.remaining() > 0) {
					// ... or the socket's buffer fills up
					break;
				}
				queue.remove(0);
			}

		}
	}


	private void finishConnection(SelectionKey key) throws IOException {
		
		SocketChannel socketChannel = (SocketChannel) key.channel();
	
		// Finish the connection. If the connection operation failed
		// this will raise an IOException.
		try {
			socketChannel.finishConnect();
		} catch (IOException e) {
			// Cancel the channel's registration with our selector
			logger.severe("PROBLEM FINISHING CONNECTION");
		}
	
		/* SENDING HANDLED IN A SEPARATE THREAD */
		/* INITIATE SENDING THREAD */
		SendingThread st = new SendingThread(msgRate, this, socketChannel, key);
		Thread sendingThread = new Thread(st);
		sendingThread.start();
		
		ClientDiagnostics cd = new ClientDiagnostics(this);
		Thread dt = new Thread(cd);
		dt.start();
		
		
		// Register an interest in writing on this channel
		key.interestOps(SelectionKey.OP_READ);
	}

}
