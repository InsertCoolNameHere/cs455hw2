package cs455.scaling.event;

import java.nio.channels.SocketChannel;

import cs455.scaling.server.Server;

public class MessageRequest implements Event{
	
	public Server callingServer;
	public byte[] payload;
	public SocketChannel sc;
	

	public MessageRequest(Server server, SocketChannel sc, byte[] dataCopy) {
		// TODO Auto-generated constructor stub
		this.callingServer = server;
		this.sc = sc;
		this.payload = dataCopy;
	}

}

