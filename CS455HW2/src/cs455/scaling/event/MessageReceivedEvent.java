package cs455.scaling.event;

import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.Map;

import cs455.scaling.server.Server;
import cs455.scaling.worker.WorkerThread;

public class MessageReceivedEvent implements Event {
	public Server callingServer;
	public SelectionKey sKey;
	public HashMap<SelectionKey, Boolean> existingKeyMap;
	

	public MessageReceivedEvent(Server server, SelectionKey sc, HashMap<SelectionKey, Boolean> existingKeyMap) {

		this.callingServer = server;
		this.sKey = sc;
		this.existingKeyMap = existingKeyMap;
	}

}
