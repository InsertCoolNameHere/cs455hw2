package cs455.scaling.worker;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.logging.Logger;

import cs455.scaling.data.SHA1Hasher;
import cs455.scaling.event.MessageReceivedEvent;
import cs455.scaling.helpers.ThreadPoolManager;
import cs455.scaling.server.Server;
import cs455.scaling.util.LogFactory;

public class WorkerThread implements Runnable {

	public MessageReceivedEvent currentEvent;
	public LinkedList threadPool;
	private static Logger logger;
	private ThreadPoolManager manager;
	
	public WorkerThread(LinkedList threadPool, ThreadPoolManager manager) {
		
		logger = LogFactory.getLogger(WorkerThread.class.getName(), "worker.out");
		this.threadPool = threadPool;
		this.manager = manager;
	}
	
	@Override
	public void run() {
		synchronized (this) {
			while (true) {
				try {

					this.wait();

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//logger.info("ABOUT TO PROCESS EVENT");
				processEvent();

			}
		}

	}
	
	private void processEvent() {
		// TODO Auto-generated method stub
		byte[] msg = null;
		try {
			msg = currentEvent.callingServer.readFromThread(currentEvent.sKey);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
			logger.severe("TROUBLE READING FROM WORKER");
		}
		
		if(msg == null) {
			//logger.info("NULL MESSAGE ENCOUNTERED");
			synchronized(threadPool) {
				threadPool.add(Thread.currentThread());
				threadPool.notify();
				//logger.info("THREADPOOL NOTIFIED");
			}
		}
		
		if(currentEvent.existingKeyMap.get(currentEvent.sKey) != null) {
			currentEvent.existingKeyMap.put(currentEvent.sKey, true);
		}
		
		if(msg != null) {
			try {
				//logger.info("RECEIVED FROM CLIENT "+msg);
				String hash = SHA1Hasher.SHA1FromBytes(msg);
				
				/* APPENDING THE SIZE UP FRONT AS WELL */
				hash = Integer.toString(hash.length())+hash;
				
				byte[] pload = hash.getBytes();
				
				SocketChannel sc = (SocketChannel)currentEvent.sKey.channel();
				currentEvent.callingServer.send(sc, pload);
				
				//logger.info("SENT TO CLIENT "+hash);
				
				// NOW GET ADDED TO THREAD POOL AND WAIT
				synchronized(threadPool) {
					threadPool.add(Thread.currentThread());
					threadPool.notify();
					//logger.info("THREADPOOL NOTIFIED");
				}
				
				manager.incrementNumProcessed();
				
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
			return;
		}
		
		
	}
	
	public static void main(String arg[]) {
		String s = "hello";
		System.out.println(s.length());
		System.out.println(s.getBytes().length);
	}
	
	

}