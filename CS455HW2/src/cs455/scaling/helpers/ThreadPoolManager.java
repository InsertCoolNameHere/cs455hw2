package cs455.scaling.helpers;

import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Logger;

import cs455.scaling.event.MessageReceivedEvent;
import cs455.scaling.event.MessageRequest;
import cs455.scaling.server.Server;
import cs455.scaling.util.LogFactory;
import cs455.scaling.worker.WorkerThread;

public class ThreadPoolManager implements Runnable{
	
	/* THIS IS A QUEUE OF ALL EVENTS RECEIVED AT THE SERVER */
	/* THIS IS TO BE POPPED AND EACH THREAD IS TO HANDLE EACH EVENT */
	private LinkedList eventQueue;
	
	/* A THREAD-POOL */
	private LinkedList threadPool;
	
	private int threadPoolSize;
	
	private static Logger logger;
	
	private int numProcessed;
	
	private HashMap<Thread, WorkerThread> threadMap;
	
	/* IF THIS BOOLEAN IS FALSE, DO NOT ADD A NEW TASK ON THIS KEY */
	private HashMap<SelectionKey, Boolean> existingKeyMap;
	
	public ThreadPoolManager(int size) {
		
		numProcessed = 0;
		
		logger = LogFactory.getLogger(ThreadPoolManager.class.getName(), "scaling-server.out");
		this.threadPoolSize = size;
		
		existingKeyMap = new HashMap<SelectionKey, Boolean>();
		
		eventQueue = new LinkedList();
		
		threadPool = new LinkedList();
		
		threadMap = new HashMap<Thread, WorkerThread>();
		
		for(int i=0; i < threadPoolSize; i++) {
			
			WorkerThread w = new WorkerThread(threadPool, this);
			Thread t = new Thread(w);
			threadPool.add(t);
			
			threadMap.put(t, w);
			
			t.start();
			
		}
		
	}
	
	public synchronized void incrementNumProcessed() {
		numProcessed++;
	}
	
	public synchronized int getNumProcessed() {
		return numProcessed;
	}

	public synchronized void setNumProcessed(int numProcessed) {
		this.numProcessed = numProcessed;
	}
	
	
	
	public void addEvent(Server callingNode, SelectionKey sKey, String operation) {
		
		/* CHECK IF THIS EVENT IS ALREADY IN THE QUEUE OR NOT FOR READ */
		/* HAVE A MAP OF KEY->BOOLEAN THAT GETS SET TO FALSE EVERYTIME A READ EVENT IS PICKED FROM QUEUE FOR A PARTICULAR KEY*/
		/* SET IT BACK TO TRUE ONCE DONE */
		
		synchronized(eventQueue) {
			
			if("READ".equals(operation) && existingKeyMap.get(sKey) != null && !existingKeyMap.get(sKey)) {
				return;
			}
		    
			/* NO MORE READING TASKS ACCEPTED ON THIS SELECTIONKEY */
			/* THIS WILL BE AGAIN SET TO TRUE WHEN A THREAD PICKS THIS UP FOR EXECUTION */
			existingKeyMap.put(sKey, false);
			
		    MessageReceivedEvent req = new MessageReceivedEvent(callingNode, sKey, existingKeyMap);
		    
		    eventQueue.add(req);
		    eventQueue.notify();
		    
		    //logger.info("DONE ADDING TO QUEUE "+eventQueue.size());
		}
		
	}
	
	/*public void addEvent(Server server, SocketChannel sc, byte[] data, int numBytesRead) {
		
		synchronized(eventQueue) {
			byte[] dataCopy = new byte[numBytesRead];
		    System.arraycopy(data, 0, dataCopy, 0, numBytesRead);
		    
		    MessageRequest req = new MessageRequest(server, sc, dataCopy);
		    
		    eventQueue.add(req);
		    eventQueue.notify();
		}
		
	}*/

	@Override
	public void run() {
		MessageReceivedEvent msgReq;
		
		while (true) {
			synchronized (eventQueue) {
				
				if (eventQueue.isEmpty()) {
					try {
						
						eventQueue.wait();
						
					} catch (InterruptedException e) {
						
						logger.severe("TROUBLE AT THREADPOOL MANAGER 1");
					}
				}
				//logger.info("OUT OF EVENTQUEUE WAIT");
				// THIS NEEDS TO BE HANDLED BY A THREAD FROM THE THREADPOOL
				//msgReq.callingServer.send(msgReq.sc, msgReq.payload);
				
				msgReq = (MessageReceivedEvent)eventQueue.pop();
				
			}
			Thread t;
			synchronized(threadPool) {
				
				//logger.info("MARKER 1");
				
				if (threadPool.isEmpty()) {
					try {
						logger.info("THREADPOOL ON WAIT");
						threadPool.wait();
						
					} catch (InterruptedException e) {
						
						logger.severe("TROUBLE AT THREADPOOL MANAGER 2");
					}
				}
				//logger.info("MARKER 2");
				
				t = (Thread)threadPool.pop();
			}
				
			WorkerThread wt = threadMap.get(t);
			
			//msgReq = (MessageReceivedEvent)eventQueue.pop();
			
			wt.currentEvent = msgReq;
			
			//logger.info("MARKER 3");
			
			synchronized(wt){
				wt.notify();
			}
				
			
			
			//logger.info("OUT OF POOL WAIT");
			
			
		}
		
	}


}



