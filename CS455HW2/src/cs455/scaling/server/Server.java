package cs455.scaling.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import cs455.scaling.helpers.ChangeRequest;
import cs455.scaling.helpers.ThreadPoolManager;
import cs455.scaling.util.LogFactory;



public class Server implements Runnable{

	private int serverPortNum;
	private int threadPoolSize;
	
	private static Logger logger;
	private ServerSocketChannel serverSocketChannel;
	private SelectionKey serverKey;
	private Selector selector;
	
	private ThreadPoolManager poolManager;
	
	private List pendingChanges;
	private Map dataChannelMap;
	
	
	
	public Server(int a, int b) {
		
		serverPortNum = a;
		threadPoolSize = b;
		logger = LogFactory.getLogger(Server.class.getName(), "scaling-server.out");
		poolManager = new ThreadPoolManager(threadPoolSize);
		Thread threadPoolThread = new Thread(poolManager);
		threadPoolThread.start();
		
		
		pendingChanges = new LinkedList();
		dataChannelMap = new HashMap();
		
		try {
			serverSocketChannel = ServerSocketChannel.open();
			
			serverSocketChannel.configureBlocking(false);
			
			InetSocketAddress localAddress = new InetSocketAddress(a);
			serverSocketChannel.bind(localAddress);
			
			selector = Selector.open();
			
			serverKey = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/*http://pastebin.com/ZQhUEHpH
	 * http://pastebin.com/KUBccYpA
	 * http://pastebin.com/d25sezWf*/
	
	public void run() {
		
		ServerDiagnostics diag = new ServerDiagnostics(poolManager, selector);
		Thread dt = new Thread(diag);
		dt.start();
		
		while(true) {
			try {
				
				/*synchronized (this.pendingChanges) {
					Iterator changes = this.pendingChanges.iterator();
					while (changes.hasNext()) {
						ChangeRequest change = (ChangeRequest) changes.next();
						switch (change.type) {
						case ChangeRequest.CHANGEOPS:
							SelectionKey key = change.socket.keyFor(this.selector);
							key.interestOps(change.ops);
						}
					}
					this.pendingChanges.clear();
				}*/
				
				selector.selectNow();
				
				Set<SelectionKey> selectedKeys = selector.selectedKeys();

				Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
				
				while(keyIterator.hasNext()) {
					
					SelectionKey sKey = keyIterator.next();
					
					if(!sKey.isValid()) {
						continue;
					}
					if(sKey.isAcceptable()) {
						/* Accepting new connections from a Client */
						logger.info("ABOUT TO ACCEPT A CLIENT");
						accept(sKey);
					} else if(sKey.isReadable()) {
						//logger.info("ABOUT TO READ FROM A CLIENT");
						/* Reading input from the client */
						read(sKey);
					} /*else if (sKey.isWritable()) {
						this.write(sKey);
					}*/
					
					keyIterator.remove();
					
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}
	
	
	public void write(SelectionKey sKey) throws IOException {
		SocketChannel socketChannel = (SocketChannel) sKey.channel();

		synchronized (this.dataChannelMap) {
			List queue = (List) this.dataChannelMap.get(socketChannel);

			// Write until there's not more data
			while (queue !=null && !queue.isEmpty()) {
				ByteBuffer buf = (ByteBuffer) queue.get(0);
				socketChannel.write(buf);
				if (buf.remaining() > 0) {
					// ... or the socket's buffer fills up
					break;
				}
				queue.remove(0);
			}

			if (queue.isEmpty()) {
				// We wrote away all data, so we're no longer interested
				// in writing on this socket. Switch back to waiting for
				// data.
				sKey.interestOps(SelectionKey.OP_READ);
			}
		}
	}
	
	
	public void write(byte[] pload, SocketChannel socketChannel) throws IOException {

		// Write until there's not more data
		ByteBuffer buf = ByteBuffer.wrap(pload);
		socketChannel.write(buf);
		
	}
	
	
	/* SIMPLY POPULATES THE PENDING CHANGES AND THE DATACHANNEL */
	/* IT IS THE JOB OF THE WRITE METHOD TO DEAL WITH THE ACTUAL SENDING */
	public void send(SocketChannel sc, byte[] payload) throws IOException {
		
		this.write(payload, sc);
		
		//synchronized(pendingChanges) {
			
			//pendingChanges.add(new ChangeRequest(sc, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));
			
			/*synchronized (this.dataChannelMap) {
				
				List queue = (List) this.dataChannelMap.get(sc);
				
				if (queue == null) {
					queue = new ArrayList();
					dataChannelMap.put(sc, queue);
				}
				
				queue.add(ByteBuffer.wrap(payload));
				
			}*/
		//}
		//selector.wakeup();
	}
	
	/* SIMPLY ADD THIS NEW EVENT TO THE EVENT QUEUE*/
	/* LET THREADPOOL DEAL WITH THIS EVENT */
	
	private void read(SelectionKey sKey) throws IOException {
		poolManager.addEvent(this, sKey, "READ");
	}
	
	
	public byte[] readFromThread(SelectionKey sKey) throws IOException {
		ByteBuffer readBuffer = ByteBuffer.allocate(8*1024);
		SocketChannel sc = (SocketChannel)sKey.channel();
		readBuffer.clear();
		
		int numBytesRead = 0;
		
		try {
			while(readBuffer.hasRemaining() && numBytesRead != -1) {
				numBytesRead = sc.read(readBuffer);
			}
			
		} catch (IOException e) {
			logger.info("Error reading from a channel on Server.");
			sKey.cancel();
			sc.close();
			return null;
		} 
		readBuffer.flip();
		// SOCKET SHUT DOWN FROM OTHER SIDE
		if(numBytesRead == -1) {
			sc.close();
			sKey.cancel();
			return null;
		}
		
		if(numBytesRead > 0) {
			return readBuffer.array();
		}
		
		return null;
		
	}

	private void accept(SelectionKey key) throws IOException {
		
	    // For an accept to be pending the channel must be a server socket channel.
	    ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

	    // Accept the connection and make it non-blocking
	    SocketChannel socketChannel = serverSocketChannel.accept();
	    
	    if(socketChannel == null)
	    	return;
	    
	    socketChannel.configureBlocking(false);

	    // Register the new SocketChannel with our Selector, indicating
	    // we'd like to be notified when there's data waiting to be read
	    socketChannel.register(this.selector, SelectionKey.OP_READ);
	    
	    logger.info("New Client Connected: "+ socketChannel.getRemoteAddress()+".\nNumber of clients connected currently: "+selector.keys().size());
	  }
	
	public static void main(String[] args) {
		if(args.length != 2) {
			System.out.println("WRONG NUMBER OF ARGUMENTS ENTERED");
			return;
		}
		
		Server s = new Server(Integer.valueOf(args[0]), Integer.valueOf(args[1]));
		Thread serverThread = new Thread(s);
		serverThread.start();
	}

}
