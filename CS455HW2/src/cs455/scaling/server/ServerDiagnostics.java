package cs455.scaling.server;

import java.nio.channels.Selector;
import java.sql.Timestamp;
import java.util.logging.Logger;

import cs455.scaling.helpers.ThreadPoolManager;
import cs455.scaling.util.LogFactory;

public class ServerDiagnostics implements Runnable {
	
	private ThreadPoolManager poolManager;
	private Selector selector;
	private Logger logger;

	public ServerDiagnostics(ThreadPoolManager poolManager, Selector selector) {
		this.poolManager = poolManager;
		this.selector = selector;
		this.logger = LogFactory.getLogger(ServerDiagnostics.class.getName(), "server-diagnostic.out");
	}

	@Override
	public void run() {
		while(true) {
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				
			}
			int numM;
			
			numM = poolManager.getNumProcessed();
			poolManager.setNumProcessed(0);
		
			int numC;
			
			numC = selector.keys().size() - 1;
			
			
			String s1 = "\n\n\n=========================================================================================================================\n";
			
			Timestamp t = new Timestamp(System.currentTimeMillis());
			
			String str = "["+ t.toString() + "] Current Server Throughput: "+ (numM/5) + " messages/s, Active Client Connections: "+numC;
			
			str = s1+str+"\n=========================================================================================================================\n\n\n";
			
			//System.out.println(str);
			
			this.logger.info(str);
			
		}

	}

}
