package cs455.scaling.data;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

import cs455.scaling.server.Server;
import cs455.scaling.util.LogFactory;

public class SHA1Hasher {
	
	public static String SHA1FromBytes(byte[] data) throws NoSuchAlgorithmException {
		
		MessageDigest digest = MessageDigest.getInstance("SHA1");
		byte[] hash = digest.digest(data);
		BigInteger hashInt = new BigInteger(1, hash);
		return hashInt.toString(16);
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException {
		Logger logger = LogFactory.getLogger(Server.class.getName(), "scaling-server.out");
		MessageDigest digest = MessageDigest.getInstance("SHA1");
		byte[] hash = digest.digest(GenerateRandomString.getString());
		BigInteger hashInt = new BigInteger(1, hash);
		System.out.println(hashInt.intValue());
		System.out.println("Good morning");
	}

}
